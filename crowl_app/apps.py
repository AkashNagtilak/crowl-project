from django.apps import AppConfig


class CrowlAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'crowl_app'
