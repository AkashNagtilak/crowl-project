# Generated by Django 4.0.4 on 2022-05-11 20:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crowl_app', '0005_searchtagssuggetions'),
    ]

    operations = [
        migrations.DeleteModel(
            name='SearchTagsSuggetions',
        ),
    ]
