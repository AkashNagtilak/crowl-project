from django import forms

class SearchTagForm(forms.Form):
    search_tag = forms.CharField(label='Search Tag', max_length=100)