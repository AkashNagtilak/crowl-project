from django.urls import path
from .views import DashBoard, SearchDatabaseTag, SearchDatabaseView, SearchTagAjax, export_csv, angular_gauge

urlpatterns = [
    path('', DashBoard.as_view(), name='dashbaord-view'),
    path('search-database-tag/', SearchDatabaseTag.as_view(), name='search-medium-tag'),
    path('search-database/', SearchDatabaseView.as_view(), name= "search-database"),
    path('search-ajax/', SearchTagAjax.as_view(), name= "search-ajax"),
    path('medium-csv/', export_csv, name='export-csv'),
    path('angular-gauge/',angular_gauge, name='angular-gauge')
]
