from django.shortcuts import render
from django.views import generic
import requests
from bs4 import BeautifulSoup
from django.conf import settings
from django.db.models import Q
from .fusioncharts import FusionCharts

# Manually added
from .models import SearchTagModel
from django.shortcuts import HttpResponse

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.template.loader import render_to_string
from django.contrib.auth.mixins import LoginRequiredMixin
import csv
from django.http import HttpResponse

# Create your views here.
class DashBoard(generic.TemplateView):
    """
        Class for DashBard View.
    """
    template_name = 'crowl_app/index.html'


class SearchDatabaseTag(generic.View):
    model = SearchTagModel
    template_name = 'crowl_app/search_tag.html'
    context_object_name = 'search_tag_medium'

    def get(self, request, *args, **kwargs):
        tags = SearchTagModel.objects.all().distinct('article_tags')
        
        context = {
            "tags": tags
        }

        return render(request, self.template_name, context)

class SearchDatabaseView(LoginRequiredMixin ,generic.ListView):
    """
        Class for search database.
    """
    model = SearchTagModel
    template_name = "crowl_app/search_tag_database.html"
    paginate_by = 6

    def get_queryset(self):

        data = self.request.GET
        school_search = data.get('school_search')

        search = data.get('search')

        qs = self.model.objects.all().order_by('-date_created')

        if search:
            if search.isnumeric():
                qs = qs.filter(id=int(search))
            else:
                qs = qs.filter(Q(title_text__icontains=search) | Q(autor_list__icontains=search) | Q(
                    article_tags__icontains=search) | Q(minite_read__icontains=search))
                # qs = qs.filter(article_tags__icontains=search)
        return qs

@method_decorator(csrf_exempt, name="dispatch")
class SearchTagAjax(generic.View):
    """
        Class for ajax search tag.
    """
    
    def post(self, request, *args, **kwargs):

        data = self.request.POST
        search = data.get('search_value').lower()
        search = search.strip()
        search = search.replace(" ", "-")

        context = dict()
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600',
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
        }
        url_link = "https://medium.com/tag/" + str(search)
        result = requests.get(url_link, headers)
        soup = BeautifulSoup(result.content, "html.parser")
        article_title = soup.find_all("h2", {"class": settings.TITALE_TEXT})
        article_paragraph = soup.find_all("p", {"class": settings.ARICLAE_PARAGRAPH})
        minitue_read = soup.find_all("p", {"class": settings.MINIT_READ})
        articles_tags = soup.find_all("div", {"class": settings.ARICLAE_TAGAS})
        author_list = soup.find_all('p', {"class": settings.AUTHOR_LIST})
        author_url = soup.find_all('a', {"class": settings.AUTHOR_URL})

        title_text = [''.join(title.findAll(text=True)) for title in article_title],
        paragrph = [''.join(paragraph.findAll(text=True)) for paragraph in article_paragraph],
        article_url = [''.join(url.get('href')) for url in author_url],
        minite_read = [''.join(minitue.findAll(text=True)) for minitue in minitue_read],
        article_tags = [''.join(article.findAll(text=True)) for article in articles_tags],
        author = [''.join(author.findAll(text=True)) for author in author_list]

        article_url = article_url[0: len(title_text)]

        all_data = list(zip(*title_text, *paragrph, *article_url, *minite_read, *article_tags, author))
        context_list = []
        for obj in all_data:
            context_list.append({
                "title": obj[0], "tags": obj[4], "creater": obj[-1], "details": obj[1], "minute_read": obj[3], 
                "url": obj[2]
            })

            SearchTagModel.objects.update_or_create(title_text=obj[0], autor_list=obj[-1], article_paragraph=obj[1], article_url=obj[2], article_tags=obj[4], minite_read=obj[3])

        context.update({
            "context_list": context_list,
        })
        html = render_to_string('crowl_app/item_table.html', context)
        return HttpResponse(html)


def export_csv(request):
    response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="medium_data.csv"'},
    )

    writer = csv.writer(response)
    writer.writerow(['Title', 'Tags', 'Details', 'Author', 'Minutes to read', 'URL', 'Created Date'])
    datas = SearchTagModel.objects.all().order_by('-id')

    for data in datas:
        writer.writerow([data.title_text, data.article_tags, data.article_paragraph, data.autor_list, data.minite_read, data.article_url, data.date_created])

    return response

def angular_gauge(request):

    # Create an object for the angualar gauge using the FusionCharts class constructor
    total = SearchTagModel.objects.all().count()
    data = total / 500
    percentage = data * 100
    percentage = str(round(percentage))

    data = {
        "chart": 
        {
            "caption": "Medium website scraping and crawling",
            "lowerLimit": "0",
            "upperLimit": "100",
            "showValue": "1",
            "numberSuffix": "%",
            "theme": "fusion",
            "showToolTip": "1"
        },
        "colorRange": {
            "color": [{
                "minValue": "0",
                "maxValue": "50",
                "code": "#F2726F"
            }, {
                "minValue": "50",
                "maxValue": "75",
                "code": "#FFC533"
            }, {
                "minValue": "100",
                "maxValue": "100",
                "code": "#62B58F"
            }]
        },
        "dials": {
            "dial": [{ "value": percentage }]
        }
    }

    angularGauge = FusionCharts("angulargauge", "ex1" , "450", "270", "chart-1", "json", data)
    # Alternatively, you can assign this string to a string variable in a separate JSON file
    # and pass the URL of that file to the `dataSource` parameter.
    return  render(request, 'crowl_app/chart.html', {'output' : angularGauge.render(),'chartTitle': 'Total Data scraping and crawling'})
