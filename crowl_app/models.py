from django.db import models

# Create your models here.
class SearchTagModel(models.Model):
    """
        class for models.
    """
    title_text = models.CharField(max_length=200)
    autor_list = models.CharField(max_length=200)
    article_paragraph = models.TextField()
    article_url = models.URLField()
    images = models.ImageField(upload_to="article_images/")
    article_tags = models.CharField(max_length=200)
    minite_read = models.CharField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title_text